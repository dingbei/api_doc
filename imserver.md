__连接服务器（获取uid）__

```
> company_id 公司ID
> uid 用户ID（可选）
> cmicrotime 客户端毫秒
< uid
< worker_name 客服工号
< worker_nickname 客服昵称
```

```bash
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d 'company_id=4&uid=55049cb57393c4784a5a&cmicrotime=12345' "http://localhost:9501/connect"
```

```json
{
  "success": 1,
  "microtime": 1463379837100,
  "data": {
    "uid": "55049cb57393c4784a5a",
    "worker_name": "1001",
    "worker_nickname": "小张"
  }
}
```

__获取消息（pull）__

```
> company_id 公司ID
> uid 用户ID
> from 对话起始数值(offset)
> worker_name 客服工号
< messages 消息
< messages.microtime 消息发送时的微秒
< messages.cmicrotime 客户端毫秒时间戳
< total 消息总量（放在下次请求的from中）
```

```bash
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d 'company_id=4&uid=55049cb57393c4784a5a&from=7&worker_name=1001' "http://localhost:9501/pull"
```

```json
{
  "success": 1,
  "microtime": 1463379991754,
  "data": {
    "messages": [
      {
        "cmd": "shake",
        "body": "",
        "microtime": 1463379605062
        "cmicrotime": 1463379605061
      }
    ],
    "total": 1
  }
}
```

__发送消息格式__

客户端发送

|行为	  		| cmd  		| body 	|示例  |
|:-- 			|:--			|:-- 		|:--  |
|连接(自动)		|connect		|[空]		|     |
|震动			|shake			|[空]		|     |
|来源			|referrer		|来源URL	|http://www.baidu.com/|
|用户浏览		|pv				|浏览页面	|http://www.photograph.com/|
|用户发送消息	|user			|用户消息内容|[smile]你好|
|首次对话URL	|firsturl		|URL		|http://www.photograph.com/|
|点击邀请		|clickinvite	|点击所在URL|http://www.photograph.com/|
|用户关闭对话	|endtalk		|关闭所在URL|http://www.photograph.com/|

服务器端发送

|行为	  		| cmd  		| body 	|示例  |
|:-- 			|:--			|:-- 		|:--  |
|客服发送消息	|worker		|客服消息内容|[smile]你好|
|自动对话		|autoreply	|消息内容	|[smile]你好|
|强制邀请		|forceinvite	|[空]		||
|标记客资		|mark			|姓名和手机拼接|姓名:xxx 手机:xxx|
|拉黑			|black			|拉黑原因	|竞争对手|



__发送消息（震动）__

```
> uid 用户ID
> company_id 公司ID
> cmd 行为类型
> worker_name 客服工号
> cmicrotime 客户端毫秒时间戳
> body 行为内容
< 空
```

```bash
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -d 'uid=55049cb57393c4784a5a&company_id=4&cmd=shake&worker_name=1001&body=&cmicrotime=1463379605061' "http://localhost:9501/send"
```

```json
{
  "success": 1,
  "microtime": 1463380141748,
  "data": []
}
```

__客服获取消息(pull)__

```
> **注意这里用json**
> company_id 公司ID
> worker_name 客服工号
> conversations {用户ID:对话起始数值}
< {uid: 对话信息}
< 	microtime 对话最后行为微秒
<	from 对话起始数值
<	conversation 对话内容
<	total 对话消息总量
```

```bash
curl -X POST -H "Content-Type: application/json" -d '{
    "company_id" :4,
    "worker_name" : 1001,
    "conversations" :{
        "55049cb57393c4784a5a" : 6,
        "28bccc357393c43c4eb1" : 1,
        "98f016857393c3989f6f" : 1,
        "067877e57393c38dca0d" : 1
    }
}' "http://localhost:9501/worker/conversations"
```

```json
{
  "success": 1,
  "microtime": 1463380356617,
  "data": {
    "55049cb57393c4784a5a": {
      "microtime": 1463380141746,
      "from": 6,
      "conversation": [
        "{\"cmd\":\"shake\",\"body\":\"\",\"microtime\":1463379539176,\"cmicrotime\":1463379539176}",
        "{\"cmd\":\"shake\",\"body\":\"\",\"microtime\":1463379605062,\"cmicrotime\":1463379605062}",
        "{\"cmd\":\"shake\",\"body\":\"\",\"microtime\":1463380141746,\"cmicrotime\":1463380141746}"
      ],
      "total": 9
    },
    "28bccc357393c43c4eb1": {
      "microtime": 1463368771807,
      "from": 1,
      "conversation": [],
      "total": 1
    },
    "98f016857393c3989f6f": {
      "microtime": 1463368761566,
      "from": 1,
      "conversation": [],
      "total": 1
    },
    "067877e57393c38dca0d": {
      "microtime": 1463368760904,
      "from": 1,
      "conversation": [],
      "total": 1
    }
  }
}
```